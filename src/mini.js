const https = require('https');
/**
 * VARS
 */
const DEBUG = false;
const BLOCK_REQUEST = true;
const EMPLOYEE_ID = '517288';
const COOKIE = 'AWARENESS=iZuTJVDGrX0mRkCOo4hEyKlEMazUb1EDa3KuZkV05lE%2FiftofyFPH1kbA%2BVEQf6MkVZwII4wizFckpgUsRi4pWOE%2FuQ3xugpee4tpPBUwBoq24yljtCvtNOZXqtt18TaT4vwvJsp1muyN5Darrw8zdKIs5EqjsmVKOs%2BBbHX3XvouYbCxMU1vzi8%2FkSdU9YxzJLd25PBGW5vf2ASBtB7Szw6IFKj%2FmUe5%2B%2B72s0%2BXdNkp5wRHFVqywlO9%2FFJl6TpKcLTPusqn8IzVsY4AJBv95e3cLT6O7VnPdvvwH5PjfyuYTkGN2NihA9zCqo%2BgHMDj9sAoXXdp4N9o80h6Ms4TS0H3RtDfvE9zXwh53%2F3thUqjj6mqAkZt%2BmHYEk0rYY1Ab8IaQc4R3OaJ9rUVVhvJrKmC%2BI0FfYEcVAcF9fTGelGoSeoX39giThOsdfHWu%2B0tQL0QiVYWeM3trIB9c3uqPVgQnhRm4i9EnRuYLPf4vw5wRz0OCgGK1dw0oelKeJI7oYl--24XtbV0h9%2FEPKGYr--fQem0gSm9GffzamuEGf4zw%3D%3D; INTEREST=fDvY9K%2F3jezVJ8Sw9wzv0r0ZdigD2CNOcoPW8XgRvERp1PvRJDqxA1EOBwYjpLiofHpt%2F%2FEbgCqZrQbN0C1y7yuJ8stnEroY76P4qIx%2Bu25ShM%2FahlgEGY2PsUgoegB9fSKEKlU1sR99yyd4Ate%2F%2BHXWYAJQ7Wbsbt%2FxBQ2fxtoAjiwtNf4Ajmyx4QIF%2FMRbcqJzag8CiiBoUF%2FYvRikNmP73z04DVq%2BGsvtJNoaFx5iTEH1sI5Fgb7Fs3QIKJ6aDAxOHPeDxNXBKB%2FUJfE5miM%2Bu6vvDJCKeTx9RGdbV5SdbczOQKz%2BRi8d2Inqgwf0peZdohzc6HjG6WjY%2FN9htZDzIgoklhwfmr8RYZpYCAS9PEHu3rnAgtkiKgHDx9he8pEx500VowqPkuM1ifjmC1JSN0BltdvoFpGXzCciSKd4SqtqsOdVCBOWNjIPuSptG9Sc2FyJ26%2BvhdXxic8HvRPNBaffwQa%2FxsCJcNoBoyDgiiJlPeyrgJ5Fv%2FuvZLqUTOKR9DKapp15U68Ov6oT1VO7aLFdkyvw1ReI4y673%2BedFxRJ6y9cxhS1uiYiSZUweLlzB%2Byu--L41wtDVOz%2BQIAL7j--lzAeVRZZ7APE64u0bCPm8g%3D%3D; GOAL=HGSizYzPKqWpOXxIIgp3x6OQ7FlRnaAG7PyMY7CDNhMQyvaboDKa27QkYlXVMoWIkv2AvPqjFTArw0jy1akTaZSocL%2FIhMC69v8QGe95geeLFFqow5JIy4xXIyAw36DUSBQT2jOzIsblPG0CZ7boFJVC6OXUpbcRkyl0JK3hi%2Bpuc8kW9NRNPf91x%2FsSwGbEur33wKFTa23nmtvtaFnPF8XOzOiJYf5WgfgsaszIwlHsuJbgKiRFsYJ82x3EEXi5UXmwoXq2w1p4IJgh9UwPs5XKYCtyOvF84XLnLK3Cgatr%2BPgZ89GTnqV7yNLUlRv24hXrl6Lkn5yc6ASbUpAk59rPgsnG9i52g9hQmpIzmNvVOYqOhRRmE20nXFlTW8uTyDhUz8LtG%2FQgYv6lH2IHQ1ELGs%2B3FNWFvQZYudWeCVPIjGg16b1A%2BlEZgHfWZycAgJaBVjcwz9EIMy73Nbjh6Z1vI9Db%2Bmv96qF7e%2BR86iXfNdNxWQJ%2BsOBfDmLcg16b6ZDGSnFEILG1QE2aXu3MSzg6bhgrTCXGaDKBTg6gAvniPXFn8wHtBLY8IElpAfX915A%3D--Owh4%2FzjORj%2B0ynUx--WmHV8YZHb4UnkAW7bLzePQ%3D%3D; _gcl_au=1.1.625727519.1683621713; _factorial_data=%7B%22company_id%22%3A24647%2C%22user_id%22%3A510269%2C%22access_id%22%3A523235%2C%22locale%22%3A%22es%22%2C%22is_factorial%22%3Afalse%7D; AMP_MKTG_0ab91ba82c=JTdCJTIycmVmZXJyZXIlMjIlM0ElMjJodHRwcyUzQSUyRiUyRmFjY291bnRzLmdvb2dsZS5lcyUyRiUyMiUyQyUyMnJlZmVycmluZ19kb21haW4lMjIlM0ElMjJhY2NvdW50cy5nb29nbGUuZXMlMjIlN0Q=; _factorial_data=; _factorial_session_v2=72a2959510054f4983633fa708034304; _clck=8xcz33|2|fdm|0|1227; _gid=GA1.2.1678777922.1690385092; _gat_gtag_UA_83128566_3=1; _ga_7MP9JB54R9=GS1.1.1690385090.8.1.1690385469.60.0.0; _ga=GA1.1.248223989.1683621713; _clsk=cre3yx|1690385470286|3|1|o.clarity.ms/collect; AMP_0ab91ba82c=JTdCJTIyb3B0T3V0JTIyJTNBZmFsc2UlMkMlMjJkZXZpY2VJZCUyMiUzQSUyMmIzOWFjNmI2LTgxMjAtNGVhZi1iM2ZjLTAxZjEwODZhNjlkMCUyMiUyQyUyMmxhc3RFdmVudFRpbWUlMjIlM0ExNjkwMzg1NDcxNjU2JTJDJTIyc2Vzc2lvbklkJTIyJTNBMTY5MDM4NTA5MDA1MSUyQyUyMnVzZXJJZCUyMiUzQTUyMzIzNSU3RA==';
const IFNONEMATCH = 'W/"a28f835c41ad1adbe21d7fa5ba0dd1c7"';
const HOURS = [8, 16]

class Factorial {
    employee;

    constructor(employee, debug) {
        this.employee = employee;
        this.debug = debug;
        this.BLOCK_REQUEST = BLOCK_REQUEST;
    }

    async getPeriod (year, month) {
        var config = {
            method: 'get',
            host: 'api.factorialhr.com',
            path: `/attendance/periods?year=${year}&month=${month}&employee_id=${this.employee}`,
            headers: { 
                'authority': 'api.factorialhr.com', 
                'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"', 
                'accept': 'application/json, text/plain, */*', 
                'x-factorial-origin': 'web', 
                'sec-ch-ua-mobile': '?0', 
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', 
                'sec-ch-ua-platform': '"macOS"', 
                'origin': 'https://app.factorialhr.com', 
                'sec-fetch-site': 'same-site', 
                'sec-fetch-mode': 'cors', 
                'sec-fetch-dest': 'empty', 
                'referer': 'https://app.factorialhr.com/', 
                'accept-language': 'es-ES,es;q=0.9,en;q=0.8,jv;q=0.7', 
                'cookie': COOKIE,
                'if-none-match': IFNONEMATCH
            }
          };

        const periods = await this.request(config);
        if (!periods || !periods.length) {
            throw new Error(`No periods found on [${year}, ${month}]`);
        }

        return periods.find(period => (period.state === 'in_progress'));
    }

    setHour(period_id, hourIn, hourOut, day) {
        const clock_in = `${hourIn}:00`;
        const clock_out = `${hourOut}:00`;

        var data = JSON.stringify({
            period_id,
            clock_in,
            clock_out,
            minutes: 0,
            day,
            observations: null,
            history: [],
            date: new Date().toISOString(),
            half_day: null
        });
        
        var config = {
            method: 'post',
            host: 'api.factorialhr.com',
            path: '/attendance/shifts',
            headers: { 
                'authority': 'api.factorialhr.com', 
                'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"', 
                'x-factorial-origin': 'web', 
                'sec-ch-ua-mobile': '?0', 
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', 
                'content-type': 'application/json;charset=UTF-8', 
                'accept': 'application/json, text/plain, */*', 
                'sec-ch-ua-platform': '"macOS"', 
                'origin': 'https://app.factorialhr.com', 
                'sec-fetch-site': 'same-site', 
                'sec-fetch-mode': 'cors', 
                'sec-fetch-dest': 'empty', 
                'referer': 'https://app.factorialhr.com/', 
                'accept-language': 'es-ES,es;q=0.9,en;q=0.8,jv;q=0.7', 
                'cookie': COOKIE
            },
            data : data
        };

        console.log(`{${new Date().toISOString()}} [Factorial] Salvado! > ${day} - (${clock_in} - ${clock_out}) Period: ${period_id}`)
        return this.request(config);        
    }

    request(params, postData) {
        return new Promise(function(resolve, reject) {
            var req = https.request(params, function(res) {
                // reject on bad status
                if (res.statusCode < 200 || res.statusCode >= 300) {
                    return reject(new Error('statusCode=' + res.statusCode));
                }
                // cumulate data
                var body = [];
                res.on('data', function(chunk) {
                    body.push(chunk);
                });
                // resolve on end
                res.on('end', function() {
                    try {
                        body = JSON.parse(Buffer.concat(body).toString());
                    } catch(e) {
                        reject(e);
                    }
                    resolve(body);
                });
            });
            // reject on request error
            req.on('error', function(err) {
                // This is not a "Second reject", just a different sort of failure
                reject(err);
            });
            if (postData) {
                req.write(postData);
            }
            // IMPORTANT
            req.end();
        });
    }
}


const main = async () => {
    if (DEBUG) {
        console.log("/****************/")
        console.log("/** DEBUG MODE **/")
        console.log("/****************/")
    }

    if (!EMPLOYEE_ID) {
        console.log("EMPLOYEE_ID not found");
        process.exit(1);
    }

    // Today
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth() + 1;
    const day = today.getDate();
    
    // Factorial
    const factorial = new Factorial(EMPLOYEE_ID, DEBUG);
    const period = await factorial.getPeriod(year, month);
    await factorial.setHour(period.id, HOURS[0], HOURS[1], day);
}

main();