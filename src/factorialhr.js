const fs = require('fs');
const dayjs = require('dayjs');
const axios = require('axios');
const { periodHeader, shiftsHeader } = require('./factorial-headers');

class Factorial {
    employee;

    constructor(employee, debug) {
        this.employee = employee;
        this.debug = debug;
    }

    async getPeriod (year, month) {
        var config = {
            method: 'get',
            url: `https://api.factorialhr.com/attendance/periods?year=${year}&month=${month}&employee_id=${this.employee}`,
            headers: periodHeader
          };
        const periods = await this.request(config);
        if (!periods || !periods.length) {
            throw new Error(`No periods found on [${year}, ${month}]`);
        }

        return periods.find(period => (period.state === 'in_progress'));
    }

    setHour(period_id, hourIn, hourOut, day) {
        const clock_in = dayjs().hour(hourIn).format("HH:00")
        const clock_out = dayjs().hour(hourOut).format("HH:00")

        var data = JSON.stringify({
            period_id,
            clock_in,
            clock_out,
            minutes: 0,
            day,
            observations: null,
            history: [],
            date: new Date().toISOString(),
            half_day: null
        });
        
        var config = {
            method: 'post',
            url: 'https://api.factorialhr.com/attendance/shifts',
            headers: shiftsHeader,
            data : data
        };

        console.log(`{${new Date().toISOString()}} [Factorial] Salvado! > ${day} - (${clock_in} - ${clock_out}) Period: ${period_id}`)
        return this.request(config, this.debug);        
    }

    getCalendar(year, month) {

        const config = {
            method: 'get',
            url: `https://api.factorialhr.com/attendance/calendar?id=${this.employee}&year=${year}&month=${month}`,
            headers: periodHeader,
        };
        console.log(`{${new Date().toISOString()}} [Factorial] Period! > year: ${year} month: ${month}`);
        return this.request(config, this.debug);
    }

    isLaborableDay(calendar, day) {
        if (!calendar) return false;
        const calendarDay = calendar.find(d => d.day === day);
        if (!calendarDay) return false;
        return calendarDay.is_laborable && !calendarDay.is_leave;
    }    

    request(config, block) {
        if (this.debug) console.info('(axios)[REQUEST]', config);
        // return;

        return axios(config).then((response) => {
            if (this.debug) console.info("(axios)[RESPONSE]", response.data);
            return response.data
        })
        .catch(function (error) {
          fs.writeFileSync(`${new Date().toISOString()}-error.log`, JSON.stringify(error, null, 2));
          console.error("[ERROR](axios)", error.message );
          throw error;
        });
    }
}


module.exports = Factorial;