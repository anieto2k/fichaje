const periodHeader = { 
    'authority': 'api.factorialhr.com', 
    'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"', 
    'accept': 'application/json, text/plain, */*', 
    'x-factorial-origin': 'web', 
    'sec-ch-ua-mobile': '?0', 
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', 
    'sec-ch-ua-platform': '"macOS"', 
    'origin': 'https://app.factorialhr.com', 
    'sec-fetch-site': 'same-site', 
    'sec-fetch-mode': 'cors', 
    'sec-fetch-dest': 'empty', 
    'referer': 'https://app.factorialhr.com/', 
    'accept-language': 'es-ES,es;q=0.9,en;q=0.8,jv;q=0.7', 
    'cookie': process.env.COOKIE,
    'if-none-match': process.env.IFNONEMATCH,
  };


  const shiftsHeader = { 
    'authority': 'api.factorialhr.com', 
    'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"', 
    'x-factorial-origin': 'web', 
    'sec-ch-ua-mobile': '?0', 
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', 
    'content-type': 'application/json;charset=UTF-8', 
    'accept': 'application/json, text/plain, */*', 
    'sec-ch-ua-platform': '"macOS"', 
    'origin': 'https://app.factorialhr.com', 
    'sec-fetch-site': 'same-site', 
    'sec-fetch-mode': 'cors', 
    'sec-fetch-dest': 'empty', 
    'referer': 'https://app.factorialhr.com/', 
    'accept-language': 'es-ES,es;q=0.9,en;q=0.8,jv;q=0.7', 
    'cookie': process.env.COOKIE
  };
  
module.exports = {
    periodHeader,
    shiftsHeader
}