require('dotenv').config()

const dayjs = require('dayjs');
const yargs = require('yargs');
const Factorial = require('./factorialhr');
const {
    validateMonth,
    validateYear,
    validateDay,
    validateHour
} = require('./validations');



module.exports = async () => {
    const debug = process.env.DEBUG === 'true';

    if (!process.env.EMPLOYEE_ID) {
        console.error("EMPLOYEE_ID not found");
        return process.exit();
    }

    const employee = process.env.EMPLOYEE_ID;
    const options = require('./options.json');
    const argv = yargs(process.argv.slice(2))
        .options(options)
        .default('m', dayjs().month() + 1)
        .default('y', dayjs().year())
        .default('d', dayjs().date())
        .check(argv => {
            if (!validateMonth(argv.m)) {
                throw new Error(`[${argv.m}] is not a valid month`)
            }
            if (!validateYear(argv.y)) {
                throw new Error(`[${argv.y}] is not a valid year`)
            }
            if (argv.d && !validateDay(argv.y, argv.m, argv.d)) {
                throw new Error(`[${[argv.y, argv.m, argv.d]}] is not a valid day`)
            }
            if (argv.r) {

                if (argv.r[1] > argv[0]) {
                    throw new Error(`[${[argv.y, argv.m, argv.r[0]]}] must be less [${[argv.y, argv.m, argv.r[1]]}] `);
                }

                if ((!validateDay(argv.y, argv.m, argv.r[0]) || !validateDay(argv.y, argv.m, argv.r[1]))) {
                    throw new Error(`[${[argv.y, argv.m, argv.r[0]]} - ${[argv.y, argv.m, argv.r[1]]}] are not a valid range`)
                }
            }

            if (argv.r && (!validateHour(argv.h[0]) || !validateDay(argv.h[1]))) {
                throw new Error(`[${[argv.h[0]]} - ${[argv.h[1]]}] are not a valid hours`)
            }
            return true;
        }).argv;

    const {month, year, day, range, hours, force} = argv;

    if (debug) 
        console.log(">>", argv);

    const factorial = new Factorial(employee, debug);
    const period = await factorial.getPeriod(year, month);
    const calendar = await factorial.getCalendar(year, month);

    if (day) {
        if (force || factorial.isLaborableDay(calendar, day)) {
            await factorial.setHour(period.id, hours[0], hours[1], day);
        } else {
            console.log(`Day ${day} is not a laborable day`);
        }
    } else if (range) {
        for (var x = range[0]; x <= range[1]; x++) {
            if (force || factorial.isLaborableDay(calendar, x)) {
                await factorial.setHour(period.id, hours[0], hours[1], x);
            } else {
                console.log(`Day ${x} is not a laborable day`);
            }
        }
    } else {
        const date = dayjs([year, month]);
        const initDate = date.startOf('month').date();
        const endDate = date.endOf('month').date();
        for (var x = initDate; x <= endDate; x++) {
            if (force || factorial.isLaborableDay(calendar, x)) {
                await factorial.setHour(period.id, hours[0], hours[1], x);
            } else {
                console.log(`Day ${x} is not a laborable day`);
            }            
        }
    }
}