const dayjs = require('dayjs');

const validateMonth = (month) => (month >= 1 && month <=12);
const validateYear = (year) => (year >= 2021);
const validateDay = (year, month, day) => (dayjs([year, month, day]).isValid());
const validateHour = (hour) => (hour >=0 && hour <=23);

module.exports = {
    validateMonth,
    validateYear,
    validateDay,
    validateHour
}
