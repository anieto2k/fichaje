# Fichaje



[TOC]

## Instalación

```shell
> git clone https://gitlab.com/anieto2k/fichaje.git
> cd fichaje
> npm install
```



Entramos en Factorial y copiamos las cabeceras de `cookie` , `employee_id`y `if-none-match` para usarlas en el fichero `.env` del proyecto.

![image-20210917200341269](./docs/factorial.jpg)



Renombramos el fichero `.env-example` a `.env` y cambiamos los valores por los datos anteriores.

## Ejecución

```shell
> node index.js opciones
```

#### Listado de opciones

- -h,-hours (**required**): Horas de entrada y salida. Ej. `-h 8 16`
- -y, -year (default: current year): Año. Ej. `-y 2021`
- -m,-month (default: current month): Mes. Ej. `-m 9`
- -d, -day (default: current day): Día. Ej. `-d 11`
- -r, -range: Rango de días. Ej. `-r 3 9`


#### Ejemplos

```shell
// Setea 8:00-16:00 en el día actual
> node index.js -h 8 16

// Setea 8:00-16:00 en el día 10 del mes y año actuales.
> node index.js -h 8 16 -d 10

// Setea 8:00-16:00 los d as 4, 5, 6, 7 y 8 del mes y año actuales.
> node index.js -h 8 16 -r 4 8

// Setea 8:00-14:00 y 16:00-18:00 en el día actual.
> node index.js -h 8 14
> node index.js -h 16 18

```





